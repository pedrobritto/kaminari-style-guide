# Kaminari Style Guides

Conjunto de ferramentas de linting, formatação automática e configurações de editor de texto para manter um padrão de código ao estilo Kaminari.

É utilizada uma combinação de **ESLint** para linting de `.js` e `.jsx`, **Stylelint** para linting de `.scss` e **Prettier** para formatação automática e opinativa de ambos.

## Início Rápido

Você provavelmente não precisará instalar nada desse repositório em novos projetos. Todas as configurações viram em conjunto com o Kaminari Boilerplate.

Isso é apenas para desenvolvimento das regras e manutenção do Kaminari Style Guides.

## Como funciona

### Linting de JavaScript com ESLint e Airbnb

[ESLint](https://eslint.org/) é um linter para arquivos JavaScript, que verifica padrões de código em busca de regras de formatação (como tamanho de indentação, tamanho máximo del inha, etc.) e regras de qualidade de código (como não permitir objetos declarados e não utilizadaos, não permitir globais implícitos, etc.).

Se houver algum trecho de código fora do padrão dessas regras, erros e avisos são exibidos com uma breve descrição do problema, ajudando a manter uma code base mais homogênea.

Primeiro instale algum **plugin do ESLint** para seu editor. Em seguida instale o ESLint no seu projeto com:

```sh
npm install --save-dev eslint
```

Agora instale as regras do [guia de estilo JavaScript do Airbnb](https://github.com/airbnb/javascript). Eles contemplatm

```sh
# Para npm 5+
npm info "eslint-config-airbnb@latest" peerDependencies
npx install-peerdeps --dev eslint-config-airbnb
```

Em seguida adicione a arquivo de configuração `.eslintrc.json`:

```json
{
  "extends": ["airbnb"]
}
```

Por fim copie o arquivo `.stylelintrc.json` para a raíz do projeto.

> Note que esse arquivo possui configurações que dependem do Prettier, então utilize apenas após a integração ter sido feita.

A versão [`eslint-config-airbnb-base`](https://www.npmjs.com/package/eslint-config-airbnb-base) não contempla plugins para linting de React. Para versão com suporte a React, ver [`eslint-config-airbnb`](https://www.npmjs.com/package/eslint-config-airbnb).

### Linting de SCSS com Stylelint

Assim como ESLint, [Stylelint](https://stylelint.io/) também é um linter, mas para arquivos `.css` e `.scss` (atualmente não há suporte para `.sass`).

Primeiro instale algum **plugin do Stylelint** para seu editor. Em seguida instale o Stylelint no seu projeto com os plugins `stylelint-scss`, para permitir regras da sintaxe utilizada em arquivos `.scss`, e `stylelint-order`, que permite definir regras de ordenação de propriedades.

```sh
npm install --save-dev stylelint stylelint-scss stylelint-order
```

Em seguida instale o conjunto de regras `stylelint-config-sass-guidelines`, que são baseadas no [Sass Guidelines](https://sass-guidelin.es/pt/).

```sh
npm install --save-dev stylelint-config-sass-guidelines
```

Por fim copie o arquivo `.stylelintrc.json` para a raíz do projeto.

> Note que esse arquivo possui configurações que dependem do Prettier, então utilize apenas após a integração ter sido feita.

### Formatação de código com Prettier

[Prettier](https://prettier.io/) é um formatador de código automático, opinativo e de pouca extensibilidade, evitando discussões sobre estilo de código. Suporta arquivos `.js`, `.json`, `.jsx`, `.ts`, `.css`, `.scss`, `.md`, `.yaml` e outros.

Primeiro instale um **plugin do Prettier** para seu editor de texto. Depois instale o Prettier no seu projeto com:

```sh
npm install prettier --save-dev --save-exact
```

É importante o `--save-exact` para evitar atualizações do Prettier no decorrer de um projeto. Isso pode causar problemas.

#### Prettier com ESLint

Para utilizar Prettier em conjunto com ESLint, é preciso instalar mais dependências:

```sh
npm install --save-dev eslint-config-prettier eslint-plugin-prettier
```

Depois é preciso adicionar o seguinte ao final da key `extends`:

```json
{
  "extends": ["plugin:prettier/recommended"]
}
```

E pronto.

Copie o arquivo de configuração `.eslint.json` na raíz deste projeto para a raiz do seu projeto.

Importante: Todas as regras de formatação do ESLint que interfiram na formatação automática do Prettier serão ignoradas, já que a prioridade é da formatação do Prettier.

#### Prettier com Stylelint

Para integrar o prettier com Stylelint é preciso instalar mais uma dependência que evita a reportagem de possível erros de formatação após a formatação feita pelo Prettier.

```sh
npm install --save-dev stylelint-config-prettier
```

E então adicionar `stylelint-config-prettier` ao array `extends` no `.stylelintrc.json`. Ele precisa ser o **útlimo ítem do array**.

```json
{
  "extends": ["stylelint-config-prettier"]
}
```

#### Testando se tudo funciona

Os pacotes `eslint-config-prettier` e `stylelint-config-prettier` possuem scripts para testar algum conflito de configuração do ESLint e Stylelint com o Prettier.

Para testar conflitos com Stylelint:

```sh
npm run stylelint-check
```

Para testar conflitos com ESLint:

```sh
npm run eslint-check
```

Se ocorrer algum erro, ele será exibido no console.

## Lista de comandos para instalação

```sh
# ESLint
npm install --save-dev eslint

# Regras do Airbnb para ESLint
npm info "eslint-config-airbnb@latest" peerDependencies
npx install-peerdeps --dev eslint-config-airbnb

# Stylelint e plugins
npm install --save-dev stylelint stylelint-scss stylelint-order

# Regras do Sass Guidelines
npm install --save-dev stylelint-config-sass-guidelines

# Prettier
npm install prettier --save-dev --save-exact

# Overrides Prettier + ESLint
npm install --save-dev eslint-config-prettier eslint-plugin-prettier

# Overrides Prettier + Stylelint
npm install --save-dev stylelint-config-prettier
```
