# Changelog

Todas as mudanças notáveis nesse projeto serão documentadas neste arquivo.

O formato é baseado no [Keep a Changelog](https://keepachangelog.com/pt-BR/1.0.0/)
e esse projeto adere ao [Semantic Versioning](https://semver.org/lang/pt-BR/).

## [0.3.0] - 2018-08-02

### Adicionado

- Add regras de linting do Airbnb para .js e .jsx.

### Modificado

- Atualiza README.

## [0.2.0] - 2018-08-02

### Adicionado

- CHANGELOG.
- TODO.
- Arquivo de configuração para Stylelint.
- Pacotes NPM para Stylelint.
- Scripts NPM para checar regras conflitantes do Stylelint e ESLint com Prettier.
- Arquivo de configuração .prettierrc.json.

### Modificado

- README agora possui instruções de instalação para Stylelint.
- Removidas regras conflitantes do ESLint com Prettier.

## [0.1.0] - 2018-08-02

### Adicionado

- README incompleto com instruções de instalação para ESLint e Prettier.
- Arquivo de configuração para ESLint e Prettier.
- Pacotes NPM para ESLint e Prettier.
- Arquivo .editorconfig como fallback para o Prettier.
