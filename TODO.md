# TODO

- Integração ESLint e Prettier não funciona corretamente. Regras do ESLint que conflitam com formatação do prettier continuam acusando erros, mesmo após seguir os 3 métodos na documentação do Prettier. A soluçãoa temporária é utilizar as configurações do .eslintrc.json em respeito às configurações do Prettier

- Adicionar opções para linting formatação de arquivos `.ts`.
